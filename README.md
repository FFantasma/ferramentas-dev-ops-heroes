# Ferramentas DevOps: dicas e truques

## Palestrantes

**Jonathan Crispiniano** - Tech Lead DevOps na Estabilis - jonathan dot oliveira at estabilis dot com 
24 anos de idade, músico por hobby, profissional na área de tecnologia apaixonado pela cultura devops e suas ferramentas, atuando como Tech Lead na Estabilis, buscando trazer o melhor e inovador para dentro de casa.
[ [Linkedin](https://www.linkedin.com/in/jonathan-oliveira-crispiniano-6a0607119) ]

**Daniel Ginês** - DevOps Executive Officer na Estabilis - daniel at estabilis dot com 
Nerd, Geek, Pai, Marido, Louco e responsável por estratégia de tecnologia, portifólio de produtos e ofertas. Responsável por cultura DevOps, Infraestrutura, Operações e Transformação Digital.
[ [Linkedin](https://www.linkedin.com/in/danielgines/) ] - [ [Instagram](https://www.instagram.com/danielgines/) ] - [ [Github](https://github.com/danielgines) ] - [ [Twitter](https://twitter.com/dgines) ] 

# Conteúdo

### **Terraform graph**

Docs - [ [Link](https://www.terraform.io/cli/commands/graph) ]

Recurso necessário - [ [Graphviz](https://graphviz.org/) ]

Código exemplo DevOps Experience - [ [Link](https://gitlab.com/estabilis/devops-experience/terraform-example) ]


### **Diagram as Code - DaC**

Website - [ [Link](https://diagrams.mingrammer.com/) ] 

Recursos necessários: [ [Python](https://www.python.org/) ] - [  [Graphviz](https://graphviz.org/) ]

Código exemplo DevOps Experience - [ [Link](https://gitlab.com/estabilis/devops-experience/diagrams-example) ]


### **Terraform backend**

Docs - [ [Link](https://www.terraform.io/language/settings/backends/s3) ] 


### **Gitlab: template terraform**

Docs - [ [Link](https://docs.gitlab.com/ee/user/infrastructure/iac/) ]


### **Snyk**

Test cli - [ [Link](https://docs.snyk.io/products/snyk-infrastructure-as-code/snyk-cli-for-infrastructure-as-code/test-your-terraform-files-with-the-cli-tool) ] 

Snyk IaC - [ [Link](https://docs.snyk.io/products/snyk-infrastructure-as-code/getting-started-snyk-iac) ]

Extensão Visual Code - [ [Link](https://marketplace.visualstudio.com/items?itemName=snyk-security.snyk-vulnerability-scanner%20Docs%20Visual%20Code%20Snyk%20-%20https://docs.snyk.io/ide-tools/visual-studio-code-extension-for-snyk-code) ]


### **StackEdit**

Website - [ [Link](https://stackedit.io/) ]


### **MobaXterm**

Website - [ [Link](https://mobaxterm.mobatek.net/) ]

## Bonus Track

Galera de TI que de forma descontraída fala verdades sobre o nosso dia a dia.
Coffops - [ [Instagram](https://www.instagram.com/coffops/) ]
